# This migration comes from colibri (originally 20121111231553)
class RemoveDisplayOnFromPaymentMethods < ActiveRecord::Migration
  def up
    remove_column :colibri_payment_methods, :display_on
  end
end
