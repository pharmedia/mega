# This migration comes from colibri_gateway (originally 20131112133401)
class MigrateStripePreferences < ActiveRecord::Migration
  def up
    Colibri::Preference.where("#{ActiveRecord::Base.connection.quote_column_name("key")} LIKE 'colibri/gateway/stripe_gateway/login%'").each do |pref|
      pref.key = pref.key.gsub('login', 'secret_key')
      pref.save
    end
  end
end
