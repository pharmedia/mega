# This migration comes from colibri (originally 20130915032339)
class AddDeletedAtToColibriStockItems < ActiveRecord::Migration
  def change
    add_column :colibri_stock_items, :deleted_at, :datetime
  end
end
