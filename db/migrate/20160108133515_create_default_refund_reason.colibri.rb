# This migration comes from colibri (originally 20140717155155)
class CreateDefaultRefundReason < ActiveRecord::Migration
  def up
    Colibri::RefundReason.create!(name: Colibri::RefundReason::RETURN_PROCESSING_REASON, mutable: false)
  end

  def down
    Colibri::RefundReason.find_by(name: Colibri::RefundReason::RETURN_PROCESSING_REASON, mutable: false).destroy
  end
end
