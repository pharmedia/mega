# This migration comes from colibri (originally 20131127001002)
class AddPositionToClassifications < ActiveRecord::Migration
  def change
    add_column :colibri_products_taxons, :position, :integer
  end
end
