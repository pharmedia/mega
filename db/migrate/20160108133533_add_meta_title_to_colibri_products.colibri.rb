# This migration comes from colibri (originally 20140827170513)
class AddMetaTitleToColibriProducts < ActiveRecord::Migration
  def change
    change_table :colibri_products do |t|
      t.string   :meta_title
    end
  end
end
