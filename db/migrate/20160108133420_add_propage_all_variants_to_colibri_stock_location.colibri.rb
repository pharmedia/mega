# This migration comes from colibri (originally 20130516151222)
class AddPropageAllVariantsToColibriStockLocation < ActiveRecord::Migration
  def change
    add_column :colibri_stock_locations, :propagate_all_variants, :boolean, default: true
  end
end
