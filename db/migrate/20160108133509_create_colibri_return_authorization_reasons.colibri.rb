# This migration comes from colibri (originally 20140713140455)
class CreateColibriReturnAuthorizationReasons < ActiveRecord::Migration
  def change
    create_table :colibri_return_authorization_reasons do |t|
      t.string :name
      t.boolean :active, default: true
      t.boolean :mutable, default: true

      t.timestamps
    end

    reversible do |direction|
      direction.up do
        Colibri::ReturnAuthorizationReason.create!(name: 'Better price available')
        Colibri::ReturnAuthorizationReason.create!(name: 'Missed estimated delivery date')
        Colibri::ReturnAuthorizationReason.create!(name: 'Missing parts or accessories')
        Colibri::ReturnAuthorizationReason.create!(name: 'Damaged/Defective')
        Colibri::ReturnAuthorizationReason.create!(name: 'Different from what was ordered')
        Colibri::ReturnAuthorizationReason.create!(name: 'Different from description')
        Colibri::ReturnAuthorizationReason.create!(name: 'No longer needed/wanted')
        Colibri::ReturnAuthorizationReason.create!(name: 'Accidental order')
        Colibri::ReturnAuthorizationReason.create!(name: 'Unauthorized purchase')
      end
    end

    add_column :colibri_return_authorizations, :return_authorization_reason_id, :integer
    add_index :colibri_return_authorizations, :return_authorization_reason_id, name: 'index_return_authorizations_on_return_authorization_reason_id'
  end
end
