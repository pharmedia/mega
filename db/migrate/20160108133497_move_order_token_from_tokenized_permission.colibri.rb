# This migration comes from colibri (originally 20140530024945)
class MoveOrderTokenFromTokenizedPermission < ActiveRecord::Migration
  class Colibri::TokenizedPermission < Colibri::Base
    belongs_to :permissable, polymorphic: true
  end

  def up
    case Colibri::Order.connection.adapter_name
    when 'SQLite'
      Colibri::Order.has_one :tokenized_permission, :as => :permissable
      Colibri::Order.includes(:tokenized_permission).each do |o|
        o.update_column :guest_token, o.tokenized_permission.token
      end
    when 'Mysql2', 'MySQL'
      execute "UPDATE colibri_orders, colibri_tokenized_permissions
               SET colibri_orders.guest_token = colibri_tokenized_permissions.token
               WHERE colibri_tokenized_permissions.permissable_id = colibri_orders.id
                  AND colibri_tokenized_permissions.permissable_type = 'Colibri::Order'"
    else
      execute "UPDATE colibri_orders
               SET guest_token = colibri_tokenized_permissions.token
               FROM colibri_tokenized_permissions
               WHERE colibri_tokenized_permissions.permissable_id = colibri_orders.id
                  AND colibri_tokenized_permissions.permissable_type = 'Colibri::Order'"
    end
  end

  def down
  end
end
