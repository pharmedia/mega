# This migration comes from colibri (originally 20140518174634)
class AddTokenToColibriOrders < ActiveRecord::Migration
  def change
    add_column :colibri_orders, :guest_token, :string
  end
end
