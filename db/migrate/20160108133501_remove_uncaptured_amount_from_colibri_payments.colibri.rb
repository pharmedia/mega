# This migration comes from colibri (originally 20140616202624)
class RemoveUncapturedAmountFromColibriPayments < ActiveRecord::Migration
  def change
    remove_column :colibri_payments, :uncaptured_amount
  end
end
