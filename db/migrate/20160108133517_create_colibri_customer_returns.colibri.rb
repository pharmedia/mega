# This migration comes from colibri (originally 20140718133010)
class CreateColibriCustomerReturns < ActiveRecord::Migration
  def change
    create_table :colibri_customer_returns do |t|
      t.string :number
      t.integer :stock_location_id
      t.timestamps
    end
  end
end
