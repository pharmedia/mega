# This migration comes from colibri (originally 20140924164824)
class AddCodeToColibriTaxCategories < ActiveRecord::Migration
  def change
    add_column :colibri_tax_categories, :tax_code, :string
  end
end
