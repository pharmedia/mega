# This migration comes from colibri (originally 20140729133613)
class AddExchangeInventoryUnitForeignKeys < ActiveRecord::Migration
  def change
    add_column :colibri_return_items, :exchange_inventory_unit_id, :integer

    add_index :colibri_return_items, :exchange_inventory_unit_id
  end
end
