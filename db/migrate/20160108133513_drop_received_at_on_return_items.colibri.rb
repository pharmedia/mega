# This migration comes from colibri (originally 20140716204111)
class DropReceivedAtOnReturnItems < ActiveRecord::Migration
  def up
    remove_column :colibri_return_items, :received_at
  end

  def down
    add_column :colibri_return_items, :received_at, :datetime
  end
end
