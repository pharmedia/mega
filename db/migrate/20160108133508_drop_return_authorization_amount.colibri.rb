# This migration comes from colibri (originally 20140710190048)
class DropReturnAuthorizationAmount < ActiveRecord::Migration
  def change
    remove_column :colibri_return_authorizations, :amount
  end
end
