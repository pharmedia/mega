# This migration comes from colibri (originally 20131120234456)
class AddUpdatedAtToVariants < ActiveRecord::Migration
  def change
    add_column :colibri_variants, :updated_at, :datetime
  end
end
