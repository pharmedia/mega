# This migration comes from colibri (originally 20130301205200)
class AddTrackingUrlToColibriShippingMethods < ActiveRecord::Migration
  def change
    add_column :colibri_shipping_methods, :tracking_url, :string
  end
end
