# This migration comes from colibri (originally 20130815024413)
class AddAdjustmentTotalToShipments < ActiveRecord::Migration
  def change
    add_column :colibri_shipments, :adjustment_total, :decimal, :precision => 10, :scale => 2, :default => 0.0
  end
end
