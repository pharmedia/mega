# This migration comes from colibri (originally 20130319082943)
class ChangeAdjustmentsAmountPrecision < ActiveRecord::Migration
  def change
   
    change_column :colibri_adjustments, :amount,  :decimal, :precision => 10, :scale => 2
                                   
  end
end
