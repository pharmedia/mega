# This migration comes from colibri (originally 20121213162028)
class AddStateToColibriAdjustments < ActiveRecord::Migration
  def change
    add_column :colibri_adjustments, :state, :string
    remove_column :colibri_adjustments, :locked
  end
end
