# This migration comes from colibri (originally 20130208032954)
class AddPrimaryToColibriProductsTaxons < ActiveRecord::Migration
  def change
    add_column :colibri_products_taxons, :id, :primary_key
  end
end
