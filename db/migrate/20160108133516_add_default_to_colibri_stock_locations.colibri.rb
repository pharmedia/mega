# This migration comes from colibri (originally 20140717185932)
class AddDefaultToColibriStockLocations < ActiveRecord::Migration
  def change
    unless column_exists? :colibri_stock_locations, :default
      add_column :colibri_stock_locations, :default, :boolean, null: false, default: false
    end
  end
end
