# This migration comes from colibri (originally 20141012083513)
class CreateColibriTaxonsPrototypes < ActiveRecord::Migration
  def change
    create_table :colibri_taxons_prototypes do |t|
      t.belongs_to :taxon, index: true
      t.belongs_to :prototype, index: true
    end
  end
end
