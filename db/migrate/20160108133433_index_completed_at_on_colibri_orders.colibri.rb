# This migration comes from colibri (originally 20130729214043)
class IndexCompletedAtOnColibriOrders < ActiveRecord::Migration
  def change
    add_index :colibri_orders, :completed_at
  end
end
