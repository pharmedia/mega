# This migration comes from colibri (originally 20130618041418)
class AddUpdatedAtToColibriCountries < ActiveRecord::Migration
  def up
    add_column :colibri_countries, :updated_at, :datetime
  end

  def down
    remove_column :colibri_countries, :updated_at
  end
end
