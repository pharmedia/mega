# This migration comes from colibri (originally 20140715182625)
class CreateColibriPromotionCategories < ActiveRecord::Migration
  def change
    create_table :colibri_promotion_categories do |t|
      t.string :name
      t.timestamps
    end

    add_column :colibri_promotions, :promotion_category_id, :integer
    add_index :colibri_promotions, :promotion_category_id
  end
end
