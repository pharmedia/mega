# This migration comes from colibri_gateway (originally 20130213222555)
class UpdateStripePaymentMethodType < ActiveRecord::Migration
  def up
    Colibri::PaymentMethod.where(:type => "Colibri::Gateway::Stripe").update_all(:type => "Colibri::Gateway::StripeGateway")
  end
  
  def down
    Colibri::PaymentMethod.where(:type => "Colibri::Gateway::StripeGateway").update_all(:type => "Colibri::Gateway::Stripe")
  end
end
