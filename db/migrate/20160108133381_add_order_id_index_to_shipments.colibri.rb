# This migration comes from colibri (originally 20130222032153)
class AddOrderIdIndexToShipments < ActiveRecord::Migration
  def change
    add_index :colibri_shipments, :order_id
  end
end
