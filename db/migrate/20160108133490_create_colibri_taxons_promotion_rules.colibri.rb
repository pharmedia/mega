# This migration comes from colibri (originally 20140318191500)
class CreateColibriTaxonsPromotionRules < ActiveRecord::Migration
  def change
    create_table :colibri_taxons_promotion_rules do |t|
      t.references :taxon, index: true
      t.references :promotion_rule, index: true
    end
  end
end
