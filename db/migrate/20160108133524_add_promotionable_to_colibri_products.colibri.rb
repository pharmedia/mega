# This migration comes from colibri (originally 20140728225422)
class AddPromotionableToColibriProducts < ActiveRecord::Migration
  def change
    add_column :colibri_products, :promotionable, :boolean, default: true
  end
end
