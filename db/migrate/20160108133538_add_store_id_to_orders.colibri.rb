# This migration comes from colibri (originally 20141009204607)
class AddStoreIdToOrders < ActiveRecord::Migration
  def change
    add_column :colibri_orders, :store_id, :integer
    if Colibri::Store.default.persisted?
      Colibri::Order.where(store_id: nil).update_all(store_id: Colibri::Store.default.id)
    end
  end
end
