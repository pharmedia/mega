# This migration comes from colibri_gateway (originally 20121017004102)
class UpdateBraintreePaymentMethodType < ActiveRecord::Migration
  def up
    Colibri::PaymentMethod.where(:type => "Colibri::Gateway::Braintree").update_all(:type => "Colibri::Gateway::BraintreeGateway")
  end
  
  def down
    Colibri::PaymentMethod.where(:type => "Colibri::Gateway::BraintreeGateway").update_all(:type => "Colibri::Gateway::Braintree")
  end
end
