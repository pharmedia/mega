# This migration comes from colibri (originally 20140805171035)
class AddDefaultToColibriCreditCards < ActiveRecord::Migration
  def change
    add_column :colibri_credit_cards, :default, :boolean, null: false, default: false
  end
end
