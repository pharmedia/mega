# This migration comes from colibri (originally 20140207093021)
class AddTaxRateIdToShippingRates < ActiveRecord::Migration
  def change
    add_column :colibri_shipping_rates, :tax_rate_id, :integer
  end
end
