# This migration comes from colibri (originally 20130307161754)
class AddDefaultQuantityToStockMovement < ActiveRecord::Migration
  def change
    change_column :colibri_stock_movements, :quantity, :integer, :default => 0
  end
end
