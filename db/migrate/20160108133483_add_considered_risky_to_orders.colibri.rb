# This migration comes from colibri (originally 20140219060952)
class AddConsideredRiskyToOrders < ActiveRecord::Migration
  def change
    add_column :colibri_orders, :considered_risky, :boolean, :default => false
  end
end
