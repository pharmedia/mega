# This migration comes from colibri (originally 20140806144901)
class AddTypeToReimbursementType < ActiveRecord::Migration
  def change
    add_column :colibri_reimbursement_types, :type, :string
    add_index :colibri_reimbursement_types, :type

    Colibri::ReimbursementType.reset_column_information
    Colibri::ReimbursementType.find_by(name: Colibri::ReimbursementType::ORIGINAL).update_attributes!(type: 'Colibri::ReimbursementType::OriginalPayment')
  end
end
