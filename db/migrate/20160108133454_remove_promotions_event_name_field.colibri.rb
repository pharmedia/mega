# This migration comes from colibri (originally 20130917024658)
class RemovePromotionsEventNameField < ActiveRecord::Migration
  def change
    remove_column :colibri_promotions, :event_name
  end
end
