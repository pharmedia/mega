# This migration comes from colibri (originally 20140204192230)
class AddAutoCaptureToPaymentMethods < ActiveRecord::Migration
  def change
    add_column :colibri_payment_methods, :auto_capture, :boolean
  end
end
