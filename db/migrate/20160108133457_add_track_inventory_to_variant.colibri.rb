# This migration comes from colibri (originally 20131026154747)
class AddTrackInventoryToVariant < ActiveRecord::Migration
  def change
    add_column :colibri_variants, :track_inventory, :boolean, :default => true
  end
end
