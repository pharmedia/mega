# This migration comes from colibri (originally 20130708052307)
class AddDeletedAtToColibriTaxRates < ActiveRecord::Migration
  def change
    add_column :colibri_tax_rates, :deleted_at, :datetime
  end
end
