# This migration comes from colibri (originally 20150707204155)
class EnableActsAsParanoidOnCalculators < ActiveRecord::Migration
  def change
    add_column :colibri_calculators, :deleted_at, :datetime
    add_index :colibri_calculators, :deleted_at
  end
end
