# This migration comes from colibri (originally 20140723004419)
class RenameColibriRefundReturnAuthorizationId < ActiveRecord::Migration
  def change
    rename_column :colibri_refunds, :return_authorization_id, :customer_return_id
  end
end
