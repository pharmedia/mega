# This migration comes from colibri (originally 20121126040517)
class AddLastIpToColibriOrders < ActiveRecord::Migration
  def change
    add_column :colibri_orders, :last_ip_address, :string
  end
end
