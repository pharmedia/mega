# This migration comes from colibri (originally 20141101231208)
class FixAdjustmentOrderPresence < ActiveRecord::Migration
  def change
    say 'Fixing adjustments without direct order reference'
    Colibri::Adjustment.where(order: nil).find_each do |adjustment|
      adjustable = adjustment.adjustable
      if adjustable.is_a? Colibri::Order
        adjustment.update_attributes!(order_id: adjustable.id)
      else
        adjustment.update_attributes!(adjustable: adjustable.order)
      end
    end
  end
end
