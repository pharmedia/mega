# This migration comes from colibri (originally 20141023005240)
class AddCounterCacheFromColibriVariantsToColibriStockItems < ActiveRecord::Migration
  def up
    add_column :colibri_variants, :stock_items_count, :integer, default: 0, null: false

    Colibri::Variant.find_each do |variant|
      Colibri::Variant.reset_counters(variant.id, :stock_items)
    end
  end

  def down
    remove_column :colibri_variants, :stock_items_count
  end
end
