# This migration comes from colibri (originally 20140731150017)
class CreateColibriReimbursementTypes < ActiveRecord::Migration
  def change
    create_table :colibri_reimbursement_types do |t|
      t.string :name
      t.boolean :active, default: true
      t.boolean :mutable, default: true

      t.timestamps
    end

    reversible do |direction|
      direction.up do
        Colibri::ReimbursementType.create!(name: Colibri::ReimbursementType::ORIGINAL)
      end
    end

    add_column :colibri_return_items, :preferred_reimbursement_type_id, :integer
    add_column :colibri_return_items, :override_reimbursement_type_id, :integer
  end
end
