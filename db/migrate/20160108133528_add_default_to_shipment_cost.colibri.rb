# This migration comes from colibri (originally 20140804185157)
class AddDefaultToShipmentCost < ActiveRecord::Migration
  def up
    change_column :colibri_shipments, :cost, :decimal, precision: 10, scale: 2, default: 0.0
    Colibri::Shipment.where(cost: nil).update_all(cost: 0)
  end

  def down
    change_column :colibri_shipments, :cost, :decimal, precision: 10, scale: 2
  end
end
