# This migration comes from colibri (originally 20140204115338)
class AddConfirmationDeliveredToColibriOrders < ActiveRecord::Migration
  def change
    add_column :colibri_orders, :confirmation_delivered, :boolean, default: false
  end
end
