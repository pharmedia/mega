# This migration comes from colibri (originally 20140927193717)
class DefaultPreTaxAmountShouldBeZero < ActiveRecord::Migration
  def change
    change_column :colibri_line_items, :pre_tax_amount, :decimal, precision: 8, scale: 2, default: 0
    change_column :colibri_shipments, :pre_tax_amount, :decimal, precision: 8, scale: 2, default: 0
  end
end
