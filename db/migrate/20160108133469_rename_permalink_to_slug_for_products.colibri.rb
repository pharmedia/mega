# This migration comes from colibri (originally 20140106224208)
class RenamePermalinkToSlugForProducts < ActiveRecord::Migration
  def change
    rename_column :colibri_products, :permalink, :slug
  end
end
