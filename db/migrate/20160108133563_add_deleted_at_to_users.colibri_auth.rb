# This migration comes from colibri_auth (originally 20140904000425)
class AddDeletedAtToUsers < ActiveRecord::Migration
  def change
    add_column :colibri_users, :deleted_at, :datetime
    add_index :colibri_users, :deleted_at
  end
end
