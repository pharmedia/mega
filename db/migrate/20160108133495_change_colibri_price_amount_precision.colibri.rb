# This migration comes from colibri (originally 20140508151342)
class ChangeColibriPriceAmountPrecision < ActiveRecord::Migration
  def change
    change_column :colibri_prices, :amount,  :decimal, :precision => 10, :scale => 2
    change_column :colibri_line_items, :price,  :decimal, :precision => 10, :scale => 2
    change_column :colibri_line_items, :cost_price,  :decimal, :precision => 10, :scale => 2
    change_column :colibri_variants, :cost_price, :decimal, :precision => 10, :scale => 2
  end
end
