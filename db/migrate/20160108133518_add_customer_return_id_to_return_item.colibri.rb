# This migration comes from colibri (originally 20140718133349)
class AddCustomerReturnIdToReturnItem < ActiveRecord::Migration
  def change
    add_column :colibri_return_items, :customer_return_id, :integer
    add_index :colibri_return_items, :customer_return_id, name: 'index_return_items_on_customer_return_id'
  end
end
