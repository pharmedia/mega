# This migration comes from colibri (originally 20141007230328)
class AddCancelAuditFieldsToColibriOrders < ActiveRecord::Migration
  def change
    add_column :colibri_orders, :canceled_at, :datetime
    add_column :colibri_orders, :canceler_id, :integer
  end
end
