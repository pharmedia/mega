# This migration comes from colibri (originally 20140713142214)
class RenameReturnAuthorizationReason < ActiveRecord::Migration
  def change
    rename_column :colibri_return_authorizations, :reason, :memo
  end
end
