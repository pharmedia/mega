# This migration comes from colibri (originally 20130226191231)
class AddStockLocationIdToColibriShipments < ActiveRecord::Migration
  def change
    add_column :colibri_shipments, :stock_location_id, :integer
  end
end
