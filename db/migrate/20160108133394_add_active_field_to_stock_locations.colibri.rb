# This migration comes from colibri (originally 20130306191917)
class AddActiveFieldToStockLocations < ActiveRecord::Migration
  def change
    add_column :colibri_stock_locations, :active, :boolean, :default => true
  end
end
