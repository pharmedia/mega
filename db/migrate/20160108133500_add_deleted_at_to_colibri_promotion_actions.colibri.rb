# This migration comes from colibri (originally 20140609201656)
class AddDeletedAtToColibriPromotionActions < ActiveRecord::Migration
  def change
    add_column :colibri_promotion_actions, :deleted_at, :datetime
    add_index :colibri_promotion_actions, :deleted_at
  end
end
