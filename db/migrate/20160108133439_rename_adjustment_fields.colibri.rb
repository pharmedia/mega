# This migration comes from colibri (originally 20130807024302)
class RenameAdjustmentFields < ActiveRecord::Migration
  def up
    remove_column :colibri_adjustments, :originator_id
    remove_column :colibri_adjustments, :originator_type

    add_column :colibri_adjustments, :order_id, :integer unless column_exists?(:colibri_adjustments, :order_id)

    # This enables the Colibri::Order#all_adjustments association to work correctly
    Colibri::Adjustment.reset_column_information
    Colibri::Adjustment.where(adjustable_type: "Colibri::Order").find_each do |adjustment|
      adjustment.update_column(:order_id, adjustment.adjustable_id)
    end
  end
end
