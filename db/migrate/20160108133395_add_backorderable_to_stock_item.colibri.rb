# This migration comes from colibri (originally 20130306195650)
class AddBackorderableToStockItem < ActiveRecord::Migration
  def change
    add_column :colibri_stock_items, :backorderable, :boolean, :default => true
  end
end
