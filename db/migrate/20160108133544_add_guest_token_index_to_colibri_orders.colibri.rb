# This migration comes from colibri (originally 20141120135441)
class AddGuestTokenIndexToColibriOrders < ActiveRecord::Migration
  def change
    add_index :colibri_orders, :guest_token
  end
end
