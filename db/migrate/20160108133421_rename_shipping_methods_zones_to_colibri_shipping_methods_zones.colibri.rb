# This migration comes from colibri (originally 20130611054351)
class RenameShippingMethodsZonesToColibriShippingMethodsZones < ActiveRecord::Migration
  def change
    rename_table :shipping_methods_zones, :colibri_shipping_methods_zones
    # If Colibri::ShippingMethod zones association was patched in
    # CreateShippingMethodZone migrations, it needs to be patched back
    Colibri::ShippingMethod.has_and_belongs_to_many :zones, :join_table => 'colibri_shipping_methods_zones',
                                                          :class_name => 'Colibri::Zone',
                                                          :foreign_key => 'shipping_method_id'
  end
end
