# This migration comes from colibri (originally 20130619012236)
class AddUpdatedAtToColibriStates < ActiveRecord::Migration
  def up
    add_column :colibri_states, :updated_at, :datetime
  end

  def down
    remove_column :colibri_states, :updated_at
  end
end
