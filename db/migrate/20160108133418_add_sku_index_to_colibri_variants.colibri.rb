# This migration comes from colibri (originally 20130514151929)
class AddSkuIndexToColibriVariants < ActiveRecord::Migration
  def change
    add_index :colibri_variants, :sku
  end
end
