# This migration comes from colibri (originally 20130813232134)
class RenameActivatorsToPromotions < ActiveRecord::Migration
  def change
    rename_table :colibri_activators, :colibri_promotions
  end
end
