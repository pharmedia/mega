# This migration comes from colibri (originally 20130611185927)
class AddUserIdIndexToColibriOrders < ActiveRecord::Migration
  def change
    add_index :colibri_orders, :user_id
  end
end
