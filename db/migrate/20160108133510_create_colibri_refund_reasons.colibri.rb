# This migration comes from colibri (originally 20140713140527)
class CreateColibriRefundReasons < ActiveRecord::Migration
  def change
    create_table :colibri_refund_reasons do |t|
      t.string :name
      t.boolean :active, default: true
      t.boolean :mutable, default: true

      t.timestamps
    end

    add_column :colibri_refunds, :refund_reason_id, :integer
    add_index :colibri_refunds, :refund_reason_id, name: 'index_refunds_on_refund_reason_id'
  end
end
