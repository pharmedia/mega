# This migration comes from colibri (originally 20140716212330)
class AddReceptionAndAcceptanceStatusToReturnItems < ActiveRecord::Migration
  def change
    add_column :colibri_return_items, :reception_status, :string
    add_column :colibri_return_items, :acceptance_status, :string
  end
end
