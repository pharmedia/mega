# This migration comes from colibri (originally 20121107194006)
class AddCurrencyToOrders < ActiveRecord::Migration
  def change
    add_column :colibri_orders, :currency, :string
  end
end
