# This migration comes from colibri (originally 20140730155938)
class AddAcceptanceStatusErrorsToReturnItem < ActiveRecord::Migration
  def change
    add_column :colibri_return_items, :acceptance_status_errors, :text
  end
end
