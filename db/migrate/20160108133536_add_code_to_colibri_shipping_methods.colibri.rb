# This migration comes from colibri (originally 20141002191113)
class AddCodeToColibriShippingMethods < ActiveRecord::Migration
  def change
    add_column :colibri_shipping_methods, :code, :string
  end
end
