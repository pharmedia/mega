# This migration comes from colibri_auth (originally 20120605211305)
class MakeUsersEmailIndexUnique < ActiveRecord::Migration
  def up
    add_index "colibri_users", ["email"], :name => "email_idx_unique", :unique => true
  end

  def down
    remove_index "colibri_users", :name => "email_idx_unique"
  end
end
