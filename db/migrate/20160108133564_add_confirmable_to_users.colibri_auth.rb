# This migration comes from colibri_auth (originally 20141002154641)
class AddConfirmableToUsers < ActiveRecord::Migration
  def change
    add_column :colibri_users, :confirmation_token, :string
    add_column :colibri_users, :confirmed_at, :datetime
    add_column :colibri_users, :confirmation_sent_at, :datetime
  end
end
