# This migration comes from colibri (originally 20130903183026)
class AddCodeToColibriPromotionRules < ActiveRecord::Migration
  def change
    add_column :colibri_promotion_rules, :code, :string
  end
end
