# This migration comes from colibri_gateway (originally 20131008221012)
class UpdatePaypalPaymentMethodType < ActiveRecord::Migration
  def up
    Colibri::PaymentMethod.where(:type => "Colibri::Gateway::PayPal").update_all(:type => "Colibri::Gateway::PayPalGateway")
  end
  
  def down
    Colibri::PaymentMethod.where(:type => "Colibri::Gateway::PayPalGateway").update_all(:type => "Colibri::Gateway::PayPal")
  end
end
