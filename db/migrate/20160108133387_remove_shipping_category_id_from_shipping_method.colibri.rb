# This migration comes from colibri (originally 20130301162745)
class RemoveShippingCategoryIdFromShippingMethod < ActiveRecord::Migration
  def change
    remove_column :colibri_shipping_methods, :shipping_category_id
  end
end
