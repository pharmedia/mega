# This migration comes from colibri (originally 20140707125621)
class RenameReturnAuthorizationInventoryUnitToReturnItems < ActiveRecord::Migration
  def change
    rename_table :colibri_return_authorization_inventory_units, :colibri_return_items
  end
end
