# This migration comes from colibri (originally 20130628021056)
class AddUniqueIndexToPermalinkOnColibriProducts < ActiveRecord::Migration
  def change
    add_index "colibri_products", ["permalink"], :name => "permalink_idx_unique", :unique => true
  end
end
