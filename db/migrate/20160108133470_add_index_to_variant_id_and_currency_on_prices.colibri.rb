# This migration comes from colibri (originally 20140120160805)
class AddIndexToVariantIdAndCurrencyOnPrices < ActiveRecord::Migration
  def change
    add_index :colibri_prices, [:variant_id, :currency]
  end
end
