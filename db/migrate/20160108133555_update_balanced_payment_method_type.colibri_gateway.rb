# This migration comes from colibri_gateway (originally 20130415222802)
class UpdateBalancedPaymentMethodType < ActiveRecord::Migration
  def up
    Colibri::PaymentMethod.where(:type => "Colibri::Gateway::Balanced").update_all(:type => "Colibri::Gateway::BalancedGateway")
  end
  
  def down
    Colibri::PaymentMethod.where(:type => "Colibri::Gateway::BalancedGateway").update_all(:type => "Colibri::Gateway::Balanced")
  end
end
