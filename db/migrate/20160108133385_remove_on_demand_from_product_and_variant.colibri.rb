# This migration comes from colibri (originally 20130228164411)
class RemoveOnDemandFromProductAndVariant < ActiveRecord::Migration
  def change
    remove_column :colibri_products, :on_demand
    remove_column :colibri_variants, :on_demand
  end
end
