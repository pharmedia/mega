source 'https://rubygems.org'

gem 'dotenv-rails', :require => 'dotenv/rails-now'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.11'
# Use mysql as the database for Active Record
gem 'mysql2', '0.3.20'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'
gem 'puma'
gem 'foreman'

gem 'grape'
gem 'grape-entity'
gem 'grape-kaminari'
gem 'grape-swagger', git: 'https://github.com/tim-vandecasteele/grape-swagger.git'
gem 'grape_logging'

gem 'rack-attack'

gem 'rails_admin'
gem 'cancancan'

gem 'devise'
# gem 'devise_token_auth'
# gem 'grape_devise_token_auth'

gem "koala"
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'

gem 'rails_admin_nestable'
gem 'ckeditor'
gem 'paperclip'
gem 'paper_trail'
gem 'geocoder'
gem 'ancestry'

gem "font-awesome-rails"
gem 'rails-i18n'

gem 'whenever', :require => false
gem 'mechanize', :require => false

gem 'sidekiq'
gem 'redis'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano3-puma'
  gem 'capistrano-foreman'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

gem 'colibri', git: 'git@bitbucket.org:pharmedia/colibri.git'
gem 'colibri_gateway', git: 'git@bitbucket.org:pharmedia/colibri_gateway.git'
gem 'colibri_auth_devise', git: 'git@bitbucket.org:pharmedia/colibri_auth_devise.git'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
