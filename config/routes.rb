Rails.application.routes.draw do

  get 'compare' => 'compare#index'
  get 'cart' => 'cart#index'
  get 'checkout' => 'checkout#index'

  get 'account' => 'account#dashboard'
  get 'account/profile' => 'account#profile'
  get 'account/orders' => 'account#orders'
  get 'account/orders/:id' => 'account#order'
  get 'account/subscriptions' => 'account#subscriptions'
  get 'account/reviews' => 'account#reviews'
  get 'account/wishlist' => 'account#wishlist'

  get 'login' => 'account#login'

  get 'about-us'=> 'static#about_us', as: 'about_us'
  get 'faq' => 'static#faq'
  get 'how-to-order' => 'static#how_to_order', as: 'how_to'
  get 'warranty' => 'static#warranty'
  get 'contact-us' => 'static#contact', as: 'contacts'
  get 'terms-and-conditions' => 'static#terms', as: 'terms'

  get 'product/quick_view' => 'product#view', as: 'product_view'
  get 'product/:id' => 'product#show'
  get 'categories' => 'category#index'
  get 'category/:id' => 'category#show'

  root 'home#index'

  devise_for :accounts

  mount Colibri::Core::Engine => '/shop', as: 'colibri'
  mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/editor', as: 'rails_admin'
end
