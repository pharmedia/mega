require 'test_helper'

class AccountControllerTest < ActionController::TestCase
  test "should get dashboard" do
    get :dashboard
    assert_response :success
  end

  test "should get profile" do
    get :profile
    assert_response :success
  end

  test "should get orders" do
    get :orders
    assert_response :success
  end

  test "should get subscriptions" do
    get :subscriptions
    assert_response :success
  end

  test "should get reviews" do
    get :reviews
    assert_response :success
  end

  test "should get wishlist" do
    get :wishlist
    assert_response :success
  end

end
