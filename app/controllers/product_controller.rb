class ProductController < ApplicationController
  def show
  end

  def view
    respond_to do |format|
      format.html { render :layout => !request.xhr? }
    end
  end
end
