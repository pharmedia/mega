class AccountController < ApplicationController
  def dashboard
  end

  def profile
  end

  def orders
  end

  def order
  end

  def subscriptions
  end

  def reviews
  end

  def wishlist
  end

  def login
    respond_to do |format|
      format.html { render :layout => !request.xhr? }
    end
  end
end
