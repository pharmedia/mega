class StaticController < ApplicationController
  def about_us
  end

  def faq
  end

  def how_to_order
  end

  def warranty
  end

  def contact
  end

  def terms
  end
end
